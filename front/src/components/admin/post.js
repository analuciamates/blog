import React, {useState, useEffect} from 'react'
import {connect} from 'react-redux'
import {getAllCategories} from '../../actions/category/categoryAction'
import { savePost } from '../../api/post'

const Post = (props)=>{

	const [categories, setCategories] = useState([])
    const [selectedValue, setSelectedValue] = useState(false)
    const [title, setTitle] = useState('')
    const [content, setContent] = useState('')

    const onSubmitForm = () => {
        let data = {    
            title,
            content
        }

        console.log(data)
        savePost(data)
            .then((res)=>{
                console.log(res)
            })
    }

    useEffect(()=>{
        setCategories(props.items.categories)
    }, [props])

	return (
		<div className="blog__post">
            <h4>Bienvenue sur la partie édition ! </h4>
            <p>Choisissez une catégorie parmi celles au-dessous : </p>
            <form
                className="blog__register__form"
                onSubmit={(e)=>{
                    e.preventDefault()
                    onSubmitForm();
                }}
            >
            <select name="categories" id="categories"
                onChange={
                    (e) => {
                        e.preventDefault();
                        setSelectedValue(true)
                    }
                }
            >
            {
                categories.map((categorie, index)=>{
                    return(
                        <option value={categorie.id} key={index}>{categorie.name}</option>
                    )
                })
            }
            </select>
            {
                selectedValue && <div>
                    <h4>Titre : </h4>
                    <input
                        type="text"
                        name="title"
                        onChange = {
                            (e) => {
                                setTitle(e.currentTarget.value)
                            }
                        }
                    />
                    <h4>Contenu : </h4>
                    <textarea 
                        type="text"
                        name="content"
                        onChange = {
                            (e) => {
                                setContent(e.currentTarget.value)
                            }
                        }
                    />
                </div>
            }
            <input className="blog__register__button" type="submit" value="Sauvegarder"/>
            </form>
		</div>
	)
}

const mapStateToProps = (store)=>{
	return {
		items: store.categories
	}
}

const mapDispatchToProps = {
	getAllCategories
}

export default connect(mapStateToProps, mapDispatchToProps)(Post)
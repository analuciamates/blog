import React from 'react';
import { Link } from 'react-router-dom';
import {connect} from "react-redux";

const Header = (props)=>{
	return (
		<ul className="blog__header">
			<div>
				<Link to="/">Accueil</Link>
				<Link to="/register">S'inscrire</Link>
				<Link to="/login">Se connecter</Link>
				{props.user.isLogged && <div>
				<Link to="/logout"><i className="fa fa-sign-out"></i></Link>
				<Link to="/post">Editer un article</Link>
				</div>
				}
			</div>
			
      	</ul>
	)
}

const mapDispatchToProps = {

}

const mapStateToProps = (store)=>{
	return {
		user: store.user
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(Header);
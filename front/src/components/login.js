import React, {useState, useEffect} from 'react';
import { Redirect, Link } from 'react-router-dom';
import LoginImg from '../assets/img/Login.png';
import {loginUser} from '../api/user';
import {connect} from 'react-redux';
import {connectUser} from '../actions/user/userAction';

const Login = (props)=>{
    

	const [email, setEmail] = useState("");
	const [password, setPassword] = useState("");
	const [redirect, setRedirect] = useState(false);
	
	const onSubmitForm = ()=>{
		let data = {
			email: email,
			password: password
		}
		
		loginUser(data)
			.then((res)=>{
				console.log(res);
				if(res.status ===200) {
					window.localStorage.setItem('saas-token', res.token);
					props.connectUser(res.user);
					setRedirect(true);
				}
			})
		
	}
	
    return (
        <div>
            {redirect && <Redirect to="/" />}
			<h1 className="c-g title2">
				Welcome!
			</h1>
			<div className="log-container bgc-bel-air">
				<div className="log-nav-container">
					<div className="bgc-bel-air log-link">
						<Link to="/login">Login :</Link>
					</div>
				</div>
				<div>
					<div className="log-container-form">
					
						<form
							className="form-trl"
							onSubmit={(e)=>{
								e.preventDefault();
								onSubmitForm();
							}}
						>
							<label>Email</label>
							<input 
								type="text" 
								name="email" 
								onChange={(e)=>{  
									setEmail(e.currentTarget.value) 
								}}
							/>
							<label>Password</label>
							<input 
								type="password" 
								name="password" 
								onChange={(e)=>{  
									setPassword(e.currentTarget.value) 
							}}/>
							<input className="button-form bgc-santa-monica" type="submit" value="Go"/>
						</form>

					</div>
					<div className="log-container-img">
						<img className="log-img" src={LoginImg} style={{marginTop: 100}} />
					</div>
				</div>
			</div>
        </div>
    )
}

    const mapStateToProps = (store)=>{
        return {
           user: store.user
       }
    }
    
    const mapDispatchToProps = {
        connectUser
    }
    
    export default connect(mapStateToProps, mapDispatchToProps)(Login);
import React, {useState, useEffect} from 'react';
import { Redirect, Link } from 'react-router-dom';
import {loginUser} from '../../api/user';
import {connect} from 'react-redux';
import {connectUser} from '../../actions/user/userAction';

const Login = (props)=>{

	const [email, setEmail] = useState("");
	const [password, setPassword] = useState("");
	const [redirect, setRedirect] = useState(false);
	
	const onSubmitForm = ()=>{
		let data = {
			email: email,
			password: password
		}
		
		loginUser(data)
			.then((res)=>{
				console.log(res);
				if(res.status ===200) {
					window.localStorage.setItem('forum-token', res.token);
					props.connectUser(res.user);
					setRedirect(true);
				}
			})
		
	}
	
    return (
            <div className="blog__connect">
                {redirect && <Redirect to="/" />}
                <h4>Se connecter</h4>
				<form
				className="blog__connect__form"
				onSubmit={(e)=>{
					e.preventDefault();
					onSubmitForm();
				}}
				>
					<label>Email</label>
					<input 
						type="text" 
						name="email" 
						onChange={(e)=>{  
							setEmail(e.currentTarget.value) 
						}}
					/>
					<label>Password</label>
					<input 
						type="password" 
						name="password" 
						onChange={(e)=>{  
							setPassword(e.currentTarget.value) 
					}}/>
					<input className="blog__register__button" type="submit" value="Envoyer"/>
					<Link to="/forgot">mot de passe oublié ?</Link>
				</form>
			</div>
    )
}

const mapStateToProps = (store)=>{
     return {
         user: store.user
     }
 }
    
const mapDispatchToProps = {
    connectUser
}
    
export default connect(mapStateToProps, mapDispatchToProps)(Login);
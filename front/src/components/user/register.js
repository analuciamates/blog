import React, {useState, useEffect} from 'react'
import { Redirect, Link } from 'react-router-dom';
import { saveUser } from '../../api/user'

const Register = (props)=>{

    const [lastName, setLastName] = useState([])
	const [firstName, setFirstName] = useState([])
    const [email, setEmail] = useState([])
	const [nickname, setNickname] = useState([])
	const [password, setPassword] = useState([])
	const [redirect, setRedirect] = useState(false)

    const onSubmitForm = ()=>{
        let data = {
            lastName,
            firstName,
            email,
            nickname,
            password
        }

        saveUser(data)
            .then((res)=>{
                console.log(res);
                if(res.status === 200) {
                    setRedirect(true);
                }
            })
    }


	return (
			<div className="blog__register">
				{redirect && <Redirect to="/login" />}
                <h4>S'enregistrer</h4>
                <form
                    className="blog__register__form"
                    onSubmit={(e)=>{
                        e.preventDefault()
                        onSubmitForm();
                    }}
                >
					<label>Nom</label>
					<input 
						type="text" 
						name="lastName" 
						onChange={(e)=>{  
							setLastName(e.currentTarget.value) 
						}}
					/>
                    <label>Prénom</label>
					<input 
						type="text" 
						name="firstName" 
						onChange={(e)=>{  
							setFirstName(e.currentTarget.value) 
						}}
					/>
                    <label>Email</label>
					<input 
						type="text" 
						name="email" 
						onChange={(e)=>{  
							setEmail(e.currentTarget.value) 
						}}
					/>
                    <label>Username</label>
					<input 
						type="text" 
						name="nickname" 
						onChange={(e)=>{  
							setNickname(e.currentTarget.value) 
						}}
					/>
					<label>Password</label>
					<input 
						type="password" 
						name="password" 
						onChange={(e)=>{  
							setPassword(e.currentTarget.value) 
					}}/>
					<input className="blog__register__button" type="submit" value="Sauvegarder"/>
                </form>
			</div>
	)
}

export default Register
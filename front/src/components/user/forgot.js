import React, {useState, useEffect} from 'react';
import { Redirect, Link } from 'react-router-dom';
import {forgotUser} from '../../api/user';
import {connect} from 'react-redux';

const Forgot = (props)=>{

	const [email, setEmail] = useState("");
	const [redirect, setRedirect] = useState(false);
	
	const onSubmitForm = ()=>{
		let data = {
			email: email
		}

		forgotUser(data)
			.then((res)=>{
				console.log(res);
				if(res.status ===200) {
					setRedirect(true);
				}
			})
	}
	
    return (
        <div>
            {redirect && <Redirect to="/login" />}
				<div className="blog__forgot">				
					<form
						className="blog__forgot__form"
						onSubmit={(e)=>{
							e.preventDefault();
							onSubmitForm();
						}}
					>
						<h4>Mot de passe oublié ?</h4>
						<label>Email</label>
						<input 
							type="text" 
							name="email" 
							onChange={(e)=>{  
								setEmail(e.currentTarget.value) 
							}}
						/>
							
						<input className="button-form bgc-santa-monica" type="submit" value="Envoyer"/>
					</form>
			    </div>
        </div>
    )
}

    const mapStateToProps = (store)=>{
        return {
           user: store.user
       }
    }
    
    const mapDispatchToProps = {
        
    }
    
    export default connect(mapStateToProps, mapDispatchToProps)(Forgot);
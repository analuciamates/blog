import React, {useState, useEffect} from 'react'
import {connect} from 'react-redux'
import {getAllCategories} from '../actions/category/categoryAction'

const Home = (props)=>{

	const [categories, setCategories] = useState([])
    
    useEffect(()=>{
        setCategories(props.items.categories)
    }, [props])

	return (
		<div className="blog__home">
			<h1>How to ..</h1>
			<h5>le forum du fait maison</h5>
			<p>Bienvenue sur le forum où on peut partager des recettes faites maison dans l'univers de la Beauté, Maison ou encore des Loisirs</p>
			{
				categories.map((category, index)=>{
					return (
						<h4 key={index}>{category.name}</h4>
					)

				})
			}
		</div>
	)
}

const mapStateToProps = (store)=>{
	return {
		items: store.categories
	}
}

const mapDispatchToProps = {
	getAllCategories
}

export default connect(mapStateToProps, mapDispatchToProps)(Home)
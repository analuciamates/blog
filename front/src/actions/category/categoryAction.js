import {GET_ALL_CATEGORIES} from './actions-types';

export const getAllCategories = (categories)=>{
    return function (dispatch){
        dispatch({
            type: GET_ALL_CATEGORIES,
            payload: categories
        })
    }
}

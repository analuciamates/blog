import axios from 'axios';
import {config} from '../config'

export const savePost = (data)=>{
    const token = window.localStorage.getItem('forum-token');
    return axios.post(config.api_url+'/api/v1/post/save', data, {headers: {'x-access-token': token}})
            .then((response)=>{
                return response.data; 
            })
}

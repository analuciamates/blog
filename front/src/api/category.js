import axios from 'axios';
import {config} from '../config'

export const getCategories = (data)=>{
    return axios.get(config.api_url+'/api/v1/category/all', data)
            .then((response)=>{
                return response.data; 
            })
}

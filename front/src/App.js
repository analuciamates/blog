import logo from './logo.svg';
import './App.scss';
import Header from './components/header';
import Home from './components/home';
import Login from './components/user/login';
import Forgot from './components/user/forgot';
import Register from './components/user/register'
import Logout from './components/logout';
import Post from './components/admin/post';
import {Switch, Route} from "react-router-dom";
import RequireDataAuth from './helpers/require-auth-data';

function App() {
  return (
    <div className="App">
      <Header />
      <Switch>
        <Route exact path="/" component={RequireDataAuth(Home)} />
        <Route exact path="/login" component={RequireDataAuth(Login)} />
        <Route exact path="/forgot" component={RequireDataAuth(Forgot)} />
        <Route exact path="/register" component={RequireDataAuth(Register)} />
        <Route exact path="/logout" component={RequireDataAuth(Logout)} />
        <Route exact path="/post" component={RequireDataAuth(Post, true)} />
      </Switch>
    </div>
  );
}

export default App;

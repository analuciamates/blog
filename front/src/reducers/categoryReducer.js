import {GET_ALL_CATEGORIES} from '../actions/category/actions-types';

const initialState = {
    categories: []
}

export default function CategoryReducer(state = initialState, action) {
    switch(action.type){
        case GET_ALL_CATEGORIES:
            return {categories: action.payload}
        break;
    }
    
    return state;
}
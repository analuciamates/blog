import { combineReducers } from "redux";
import UserReducer from './userReducer';
import CategoryReducer from "./categoryReducer";

const rootReducer = combineReducers({
    user: UserReducer,
    categories: CategoryReducer
})

export default rootReducer;
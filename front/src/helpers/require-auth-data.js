import React from 'react';
import {connect} from 'react-redux';
import {Link, Redirect} from 'react-router-dom';
import {getCategories} from '../api/category';
import {getAllCategories} from '../actions/category/categoryAction'
import {checkToken} from '../api/auth';

export default function(ChildComponent, withAuth=false) {
    class RequireDataAuth extends React.Component {
        
        constructor(props){
            super(props);
            this.state = {
                redirect: false
            }
        }
        
        componentDidMount(){

            if(this.props.items.categories.length === 0) {   
                console.log('JE CHARGE LES CATEGORIES')
                getCategories()
                    .then((res)=>{
                        console.log(res);
                        this.props.getAllCategories(res.categories);
                    });
            }

            const token = window.localStorage.getItem('my-token');

            // je teste si l'utilisateur est connecté si oui il a accès à tout
            if(this.props.user.isLogged === false) {
                // si pas connecté pas de token dans le local storage
                
                if(token === null) {
                    // si besoin d'etre connecté alors redirigé
                    if(withAuth) {
                     this.setState({redirect: true})
                    }
                } else {
                    // si il y a un token on teste
                    checkToken()
                    .then((res)=>{
                       console.log(res); 
                       // si le token est mauvais on redirige
                       if(res.status !== 200) {
                           if(withAuth) {
                              this.setState({redirect: true}) 
                           }
                       } else {
                           // si le token est bon on charge les infos redux et on reste sur la page
                          
                       }
                    })
                }
                
            }
        }
                
        render(){
            if(this.state.redirect) {
                return <Redirect to="/login"/>
            }
            
            return (
                <ChildComponent {...this.props} />  
            )
        } 
    }

    const mapStateToProps = (store)=>{
        return {
           user: store.user,
           items: store.categories
       }
    }
    
    const mapDispatchToProps = {
        getAllCategories
    }
    
    return connect(mapStateToProps, mapDispatchToProps)(RequireDataAuth);
}

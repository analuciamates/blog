const express = require('express');
const app = express();
app.use(express.static(__dirname + '/public'));

const bodyParser = require('body-parser');
app.use(bodyParser.json()); 
app.use(bodyParser.urlencoded({ extended: true }));

const cors = require('cors');
app.use(cors());

const mysql = require('promise-mysql');

app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())

const userRoutes = require('./routes/userRoutes');
const authRoutes = require('./routes/authRoutes');
const categoryRoutes = require('./routes/categoryRoutes');
const commentRoutes = require('./routes/commentRoutes');
const postRoutes = require('./routes/postRoutes');

mysql.createConnection({
	/*host: "us-cdbr-east-04.cleardb.com",
	database: "heroku_a36ed71d4efe008",
	user: "bd36c714c3b6f8",
	password: "32155361",
	port: 3306*/
	host: "localhost",
	database: "blogalm",
	user: "root",
	password: "root",
	port: 8889
}).then((db) => {
	setInterval(async function () {
		let res = await db.query('SELECT 1');
	}, 10000);
	console.log("connecté bdd");
	app.get('/', (req, res, next)=>{
		res.json({msg: 'api ok', status: 200})
	})

	userRoutes(app, db)
	authRoutes(app, db)
	categoryRoutes(app, db)
	commentRoutes(app, db)
	postRoutes(app,db)
})

const PORT = process.env.PORT || 9100;

app.listen(PORT, ()=>{
	console.log('listening port: '+PORT);
	
})
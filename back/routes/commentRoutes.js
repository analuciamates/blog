const withAuth = require('../withAuth')

module.exports = (app, db)=>{
    
    const commentModel = require('../models/CommentModel')(db);
    
    app.get('/api/v1/comment/all', async (req, res, next)=>{
	    let comments = await commentModel.getAllComments();
	    if(comments.code){
	        res.json({status: 500, err: comments})
	    }
	    
	    res.json({status: 200, comments: comments})
	})
	
	app.get('/api/v1/comment/one/:id', async (req, res, next)=>{	    
	    let id = req.params.id;
	    let comment = await commentModel.getOneComment(id);
	    
	    if(comment.code){
	        res.json({status: 500, err: comment})
	    }
	    
	    res.json({status: 200, comment: comment})
	})
	
	app.post('/api/v1/comment/save', withAuth, async (req, res, next)=>{
	    let result = await commentModel.saveOneComment(req);
	    let comments = await commentModel.getAllComments();
	    
	    if(result.code){
	        res.json({status: 500, err: result})
	    }
	    
	    if(comments.code){
	        res.json({status: 500, err: comments})
	    }
	    
	    res.json({status: 200, result: result, comments: comments})
	});
	
	app.put('/api/v1/comment/update/:id', withAuth, async (req, res, next)=>{
	    let id = req.params.id;
	    let result = await commentModel.updateOneComment(req, id);
	    let comments = await commentModel.getAllComments();
	    
	    if(result.code){
	        res.json({status: 500, err: result})
	    }
	    
	    if(comments.code){
	        res.json({status: 500, err: comments})
	    }
	    
	    res.json({status: 200, result: result, comments: comments})
	});
	
	app.delete('/api/v1/comment/delete/:id', withAuth, async (req, res, next)=>{
	    let id = req.params.id;
	    let result = await commentModel.deleteOneComment(id);
	    let comments = await commentModel.getAllComments();
	    
	    if(result.code){
	        res.json({status: 500, err: result})
	    }
	    
	    if(comments.code){
	        res.json({status: 500, err: comments})
	    }
	    
	    res.json({status: 200, result: result, comments: comments})
	});	
}
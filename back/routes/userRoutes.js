const bcrypt = require('bcrypt');
const saltRounds = 10;
const jwt = require('jsonwebtoken');
const secret = "groschat";
const mail = require('../lib/mailing');
const randomString = require('random-string');

module.exports = (app, db)=>{
	const userModel = require('../models/UserModel')(db);
	
    app.post('/api/v1/user/add', async (req,  res, next)=>{
        let result = await userModel.saveOneUser(req);
		console.log(result);
		
		if(result.code){
		    res.json({status: 500, err: result.code})
		}
		
		res.json({status: 200, msg: 'user enregistré'})
	})

	app.post('/api/v1/user/login', async (req,  res, next)=>{
        let user = await userModel.getUserByMail(req.body.email);
        if(user.length === 0) {
            res.json({status: 404, msg: "email inexistant dans la base de donnée"})
        } else {
            if(user[0].validate === "no") {
                res.json({status: 403, msg: "Votre compte n'est pas validé"})
            }
    
            let same = await bcrypt.compare(req.body.password, user[0].password);
            if(same) {
    
                let infos = {id: user[0].id, email: user[0].email}
                let token = jwt.sign(infos, secret);
    
                res.json({status: 200, msg: "connecté", token: token, user: user[0]})   
    
            } else {
                res.json({status: 401, msg: "mauvais mot de passe"})
            }
        }
        
    })

    app.post('/api/v1/user/forgot', async (req,  res, next)=>{
        const password = randomString({length: 20});
        console.log("Password", password);
        let result = await userModel.updatePassword(password, req.body.email);
        let text = "Ton nouveau mot de passe est : " + password;
        console.log('TEXT', text);
        mail(req.body.email,"Nouveau mot de passe", "Ton nouveau mot de passe", text);
        res.json({status: 200, msg: "nouveau mot de passe envoyé"})
    })    

}
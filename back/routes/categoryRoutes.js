const withAuth = require('../withAuth')

module.exports = (app, db)=>{
    
    const categoryModel = require('../models/CategoryModel')(db);
    
    app.get('/api/v1/category/all', async (req, res, next)=>{
	    let categories = await categoryModel.getAllCategories();
	    if(categories.code){
	        res.json({status: 500, err: categories})
	    }
	    
	    res.json({status: 200, categories: categories})
	})
	
	app.get('/api/v1/category/one/:id', async (req, res, next)=>{	    
	    let id = req.params.id;
	    let category = await categoryModel.getOneCategory(id);
	    
	    if(category.code){
	        res.json({status: 500, err: category})
	    }
	    
	    res.json({status: 200, category: category})
	})
	
	app.post('/api/v1/category/save', withAuth, async (req, res, next)=>{
	    let result = await categoryModel.saveOneCategory(req);
	    let categories = await categoryModel.getAllCategories();
	    
	    if(result.code){
	        res.json({status: 500, err: result})
	    }
	    
	    if(categories.code){
	        res.json({status: 500, err: categories})
	    }
	    
	    res.json({status: 200, result: result, categories: categories})
	});
	
	app.put('/api/v1/category/update/:id', withAuth, async (req, res, next)=>{
	    let id = req.params.id;
	    let result = await categoryModel.updateOneCategory(req, id);
	    let categories = await categoryModel.getAllCategories();
	    
	    if(result.code){
	        res.json({status: 500, err: result})
	    }
	    
	    if(categories.code){
	        res.json({status: 500, err: categories})
	    }
	    
	    res.json({status: 200, result: result, categories: categories})
	});
	
	app.delete('/api/v1/category/delete/:id', withAuth, async (req, res, next)=>{
	    let id = req.params.id;
	    let result = await categoryModel.deleteOneCategory(id);
	    let categories = await categoryModel.getAllCategories();
	    
	    if(result.code){
	        res.json({status: 500, err: result})
	    }
	    
	    if(categories.code){
	        res.json({status: 500, err: categories})
	    }
	    
	    res.json({status: 200, result: result, categories: categories})
	});	
}
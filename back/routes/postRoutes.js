const withAuth = require('../withAuth')

module.exports = (app, db)=>{
    
    const postModel = require('../models/PostModel')(db);
    
    app.get('/api/v1/post/all', async (req, res, next)=>{
	    let posts = await postModel.getAllPosts();
	    if(posts.code){
	        res.json({status: 500, err: posts})
	    }
	    
	    res.json({status: 200, posts: posts})
	})
	
	app.get('/api/v1/post/one/:id', async (req, res, next)=>{	    
	    let id = req.params.id;
	    let post = await postModel.getOnePost(id);
	    
	    if(post.code){
	        res.json({status: 500, err: post})
	    }
	    
	    res.json({status: 200, post: post})
	})
	
	app.post('/api/v1/post/save', withAuth, async (req, res, next)=>{
	    let result = await postModel.saveOnePost(req);
	    let posts = await postModel.getAllPosts();
	    
	    if(result.code){
	        res.json({status: 500, err: result})
	    }
	    
	    if(posts.code){
	        res.json({status: 500, err: posts})
	    }
	    
	    res.json({status: 200, result: result, posts: posts})
	});
	
	app.put('/api/v1/post/update/:id', withAuth, async (req, res, next)=>{
	    let id = req.params.id;
	    let result = await postModel.updateOnePost(req, id);
	    let posts = await postModel.getAllPosts();
	    
	    if(result.code){
	        res.json({status: 500, err: result})
	    }
	    
	    if(posts.code){
	        res.json({status: 500, err: posts})
	    }
	    
	    res.json({status: 200, result: result, posts: posts})
	});
	
	app.delete('/api/v1/post/delete/:id', withAuth, async (req, res, next)=>{
	    let id = req.params.id;
	    let result = await postModel.deleteOnePost(id);
	    let posts = await postModel.getAllPosts();
	    
	    if(result.code){
	        res.json({status: 500, err: result})
	    }
	    
	    if(posts.code){
	        res.json({status: 500, err: posts})
	    }
	    
	    res.json({status: 200, result: result, posts: posts})
	});	
}
module.exports = (_db)=>{
    db = _db;
    return CommentModel;
}

class CommentModel {
    
    static getAllComments() {
        return db.query('SELECT * FROM comment')
                .then((response)=>{
                    return response
                })
                .catch((err)=>{
                    return err
                })
    }
    
    static getOneComment(id) {
        return db.query('SELECT * FROM comment WHERE id = ?', [id])
                .then((response)=>{
                    return response
                })
                .catch((err)=>{
                    return err
                })
    }
    
    static saveOneComment(req){
        return db.query('INSERT INTO comment (post_id, user_id, content, creation_date, modification_date) VALUES (?, ?, ?, NOW(), NOW())', [req.body.postId, req.body.userId, req.body.content])
                .then((response)=>{
                    return response
                })
                .catch((err)=>{
                    return err
                })
    }
    
    static updateOneComment(req, id){
        return db.query('UPDATE comment SET content = ?, modification_date = NOW() WHERE id = ?', [req.body.content, id])
                .then((response)=>{
                    return response
                })
                .catch((err)=>{
                    return err
                })
    }
    
    static deleteOneComment(id){
        return db.query('DELETE FROM comment WHERE id = ?', [id])
                .then((response)=>{
                    return response
                })
                .catch((err)=>{
                    return err
                })
    }    
}
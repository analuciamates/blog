module.exports = (_db)=>{
    db = _db;
    return CategoryModel;
}

class CategoryModel {
    
    static getAllCategories() {
        return db.query('SELECT * FROM category')
                .then((response)=>{
                    return response
                })
                .catch((err)=>{
                    return err
                })
    }
    
    static getOneCategory(id) {
        return db.query('SELECT * FROM category WHERE id = ?', [id])
                .then((response)=>{
                    return response
                })
                .catch((err)=>{
                    return err
                })
    }
    
    static saveOneCategory(req){
        return db.query('INSERT INTO category (name, creation_date, modification_date) VALUES (?, NOW(), NOW())', [req.body.name])
                .then((response)=>{
                    return response
                })
                .catch((err)=>{
                    return err
                })
    }
    
    static updateOneCategory(req, id){
        return db.query('UPDATE category SET name = ?, modification_date = NOW() WHERE id = ?', [req.body.name, id])
                .then((response)=>{
                    return response
                })
                .catch((err)=>{
                    return err
                })
    }
    
    static deleteOneCategory(id){
        return db.query('DELETE FROM category WHERE id = ?', [id])
                .then((response)=>{
                    return response
                })
                .catch((err)=>{
                    return err
                })
    }    
}
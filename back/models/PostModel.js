module.exports = (_db)=>{
    db = _db;
    return PostModel;
}

class PostModel {
    
    static getAllPosts() {
        return db.query('SELECT * FROM post')
                .then((response)=>{
                    return response
                })
                .catch((err)=>{
                    return err
                })
    }
    
    static getOnePost(id) {
        return db.query('SELECT * FROM post WHERE id = ?', [id])
                .then((response)=>{
                    return response
                })
                .catch((err)=>{
                    return err
                })
    }
    
    static saveOnePost(req){
        return db.query('INSERT INTO post (user_id, title, content, creation_date, modification_date) VALUES (?, ?, ?, NOW(), NOW())', [req.body.userId, req.body.title, req.body.content])
                .then((response)=>{
                    return response
                })
                .catch((err)=>{
                    return err
                })
    }
    
    static updateOnePost(req, id){
        return db.query('UPDATE post SET content = ?, modification_date = NOW() WHERE id = ?', [req.body.content, id])
                .then((response)=>{
                    return response
                })
                .catch((err)=>{
                    return err
                })
    }
    
    static deleteOnePost(id){
        return db.query('DELETE FROM post WHERE id = ?', [id])
                .then((response)=>{
                    return response
                })
                .catch((err)=>{
                    return err
                })
    }    
}
const bcrypt = require('bcrypt');
const saltRounds = 10;

module.exports = (_db)=>{
    db = _db;
    return UserModel;
}

class UserModel {
	static async saveOneUser(req){
		
		let hash = await bcrypt.hash(req.body.password, saltRounds);
		return db.query('INSERT INTO user (first_name, last_name, email, password, nickname, role, active, creation_date, modification_date) VALUES (?, ?, ?, ?, ?, "user", 1, NOW(), NOW())', [req.body.firstName, req.body.lastName, req.body.email, hash, req.body.nickname])
				.then((result)=>{
					return result;
				})
				.catch((err)=>{
					return err
				})
	}

	static async getUserByMail(email){
		let user = await db.query('SELECT * FROM user WHERE email = ?', [email]);

		return user;
	}

	static async getOneUser(id){
		let user = await db.query('SELECT * FROM user WHERE id = ?', [id]);

		return user;
	}

	static async updatePassword(password, email){
		let hash = await bcrypt.hash(password, saltRounds);
		let result = await db.query('UPDATE user SET password = ? WHERE email = ?', [hash, email]);

		return result;
	}

}